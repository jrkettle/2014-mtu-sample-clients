#ifndef __AGENT_H
#define __AGENT_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "state.h"

struct move {
    bool wait;
    uint8_t x, y, z;
};

extern void move_reset(struct move *move);
extern void agent_request_move(struct state *state, struct move *move);
extern void agent_move_result(struct state *state, struct move *claimed);
extern void agent_game_result(struct state *state, int winner);

#endif
