#include <stdio.h>

#include "state.h"

void state_reset(struct state *state)
{
	memset(state, 0, sizeof(struct state));
}

void state_board_init(struct state *state, uint8_t characteristic_size)
{
    int x, y;

    if (characteristic_size == state->board_characteristic_size)
        return;

    state_board_destroy(state);

    assert(characteristic_size != 0);
    state->board_characteristic_size = characteristic_size;

    state->board = (uint8_t***)malloc(sizeof(uint8_t*) * characteristic_size);
    for (x = 0; x < characteristic_size; x++) {
        state->board[x] = (uint8_t**)malloc(sizeof(uint8_t*) * (characteristic_size - x));
        for (y = 0; y < characteristic_size - x; y++) {
            //fprintf(stderr, "Allocating: %u, %u: size %u\n", x, y, characteristic_size - x - y);
            state->board[x][y] = (uint8_t*)malloc(sizeof(uint8_t) * (characteristic_size - x - y));
        }
    }
}

void state_board_destroy(struct state *state) {
    int x, y;

    if (state->board) {
        for (x = 0; x < state->board_characteristic_size; x++) {
            for (y = 0; y < (state->board_characteristic_size - x); y++) {
                free(state->board[x][y]);
            }
            free(state->board[x]);
        }
        free(state->board);

        state->board = NULL;
    }
}

void state_legal_moves_init(struct state *state, uint32_t move_count)
{
    int i;

    if (move_count == state->nlegal_moves)
        return;

    state_legal_moves_destroy(state);

    state->nlegal_moves = move_count;

    state->legal_moves = (uint8_t**)malloc(sizeof(uint8_t*) * state->nlegal_moves);
    for (i = 0; i < state->nlegal_moves; i++) {
        state->legal_moves[i] = (uint8_t*)malloc(sizeof(uint8_t) * 3);
    }
}

void state_legal_moves_destroy(struct state *state) {
    int i;

    if (state->legal_moves) {
        for (i = 0; i < state->nlegal_moves; i++) {
            free(state->legal_moves[i]);
        }
        free(state->legal_moves);

        state->legal_moves = NULL;
    }
}
