#ifndef __STATE_H
#define __STATE_H

#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

#include "config.h"
#include "error.h"
#include "types.h"

enum result {
	RESULT_WIN = 0,
	RESULT_LOSS,
	RESULT_TIE,
	RESULT_BOGUS,
};

struct state {
	uint32_t gameid;
	uint32_t playerid; /* player *number*, not team ID */
	uint32_t opponentid; /* opponent team ID */

	uint32_t my_points;
	uint32_t opp_points;

    uint32_t my_tokens;
    uint32_t opp_tokens;
    uint64_t time_remaining_ns;
    uint32_t moves_remaining;

    uint8_t  ***board;
    uint32_t board_characteristic_size;

    uint8_t **legal_moves;
    uint32_t nlegal_moves;
};

extern void state_reset(struct state *state);
extern void state_board_init(struct state *state, uint8_t characteristic_size);
extern void state_board_destroy(struct state *state);
extern void state_legal_moves_init(struct state *state, uint32_t move_count);
extern void state_legal_moves_destroy(struct state *state);

#endif
