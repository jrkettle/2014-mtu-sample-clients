#ifndef __ERROR_H
#define __ERROR_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

/* takes/returns negated errnos */
#define ERR_PTR(x)	__err_ptr(x)
#define PTR_ERR(x)	__ptr_err(x)
#define IS_ERR(x)	__is_err(x)

static inline void *__err_ptr(int x)
{
	assert(x <= 0);
	return (void*)(uintptr_t) x;
}

static inline intptr_t __ptr_err(void *x)
{
	return (intptr_t) x;
}

static inline int __is_err(void *x)
{
	intptr_t ret;

	ret = PTR_ERR(x);

	return (ret < 0) && (ret > -16*1024);
}

static inline const char *mstrerror(int ret)
{
	/* convert to positive errno */
	if (ret < 0)
		ret = -ret;

	return strerror(ret);
}

#endif
