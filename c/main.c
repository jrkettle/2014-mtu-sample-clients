#include "state.h"
#include "sock.h"
#include "rpc.h"
#include <stdlib.h>
#include <time.h>

static int connect_to_server(const char *host, uint16_t port)
{
	fprintf(stderr, "Connecting to %s:%u...\n", host, port);

	return mconnect(host, port, true, true);
}

static void service_connection(int conn)
{
	struct state state;
	int ret;

    state_reset(&state);

	for (;;) {
		ret = process_request(conn, &state);
		if (ret) {
			fprintf(stderr, "Error processing a request: %s\n",
				mstrerror(ret));
			break;
		}
	}

    state_board_destroy(&state);
    state_legal_moves_destroy(&state);
}

static void usage(char *pgm)
{
	fprintf(stderr, "Usage: %s [<host> [<port>]]\n", pgm);
	exit(1);
}

int main(int argc, char **argv)
{
	char *host;
	uint16_t port;
	int conn;

	switch (argc) {
		case 1:
			host = CONTEST_SERVER_IP;
			port = CONTEST_SERVER_PORT;
			break;
		case 2:
			host = argv[1];
			port = CONTEST_SERVER_PORT;
			break;
		case 3:
			host = argv[1];
			port = atoi(argv[2]);
			break;
		default:
			usage(argv[0]);
	}

    srand(time(NULL));

	for (;;) {
		conn = connect_to_server(host, port);
		if (conn < 0) {
			fprintf(stderr, "Connection failed: %s\n",
				mstrerror(conn));
			fprintf(stderr, "Waiting for %d seconds before "
				"reconnecting...\n", RECONNECT_DELAY);
			sleep(RECONNECT_DELAY);
			continue;
		}

		fprintf(stderr, "Connected.\n");

		service_connection(conn);

		close(conn);

		fprintf(stderr, "Connection lost.\n");
		fprintf(stderr, "Waiting for %d seconds before "
			"reconnecting...\n", RECONNECT_DELAY);
		sleep(RECONNECT_DELAY);
	}
}
