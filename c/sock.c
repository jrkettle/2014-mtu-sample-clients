#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "sock.h"

int mconnect_unix(const char *path)
{
	struct sockaddr_un un;
	int sock;
	int ret;
	int len;

	if (!path)
		return -EINVAL;

	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sock == -1)
		return -errno;

	un.sun_family = AF_UNIX;
	strncpy(un.sun_path, path, sizeof(un.sun_path));
	len = strlen(un.sun_path) + sizeof(un.sun_family);

	ret = connect(sock, (struct sockaddr *) &un, len);
	if (ret == -1) {
		ret = -errno;
		close(sock);
		return ret;
	}

	return sock;
}

int mconnect(const char *host, uint16_t port, bool v4, bool v6)
{
	struct addrinfo hints;
	struct addrinfo *res, *p;
	char cport[10];
	int sock;

	if (!host || !port || (!v4 && !v6))
		return -EINVAL;

	snprintf(cport, sizeof(cport), "%hu", port);

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(host, cport, &hints, &res))
		return -errno;

	sock = -ENOENT;

	for (p = res; p; p = p->ai_next) {
		if (p->ai_family == AF_INET) {
			if (!v4)
				continue;
		} else if (p->ai_family == AF_INET6) {
			if (!v6)
				continue;
		} else {
			continue;
		}

		if ((sock = socket(p->ai_family, p->ai_socktype,
				   p->ai_protocol)) == -1) {
			sock = -errno;
			continue;
		}

		if (connect(sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock);
			sock = -errno;
			continue;
		}

		break;
	}

	freeaddrinfo(res);

	return sock;
}
