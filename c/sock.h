#ifndef __MOEBIUS_SOCK
#define __MOEBIUS_SOCK

#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

extern int mconnect(const char *host, uint16_t port, bool v4, bool v6);

extern int sendbuf(int fd, void *buf, size_t len);
extern void *recvbuf(int fd, size_t *len);

#endif
