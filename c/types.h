#ifndef __MOEBIUS_TYPES_H
#define __MOEBIUS_TYPES_H

#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <stdint.h>

#include "config.h"

#ifdef HAVE_SYS_BYTEORDER_H
#include <sys/byteorder.h>
#endif

#define min(x, y)	({ \
				typeof(x) _m1 = (x); \
				typeof(y) _m2 = (y); \
				(void) (&_m1 == &_m2); \
				(_m1 < _m2) ? _m1 : _m2; \
			})
#define max(x, y)	({ \
				typeof(x) _m1 = (x); \
				typeof(y) _m2 = (y); \
				(void) (&_m1 == &_m2); \
				(_m1 > _m2) ? _m1 : _m2; \
			})

/* make x fit in the range [y, z] */
#define clamp(x, y, z)	({ \
				typeof(x) _m1 = (x); \
				typeof(y) _m2 = (y); \
				typeof(z) _m3 = (z); \
				(void) (&_m1 == &_m2); \
				(void) (&_m2 == &_m3); \
				(_m1 < _m2) ? _m2 : ((_m1 > _m3) ? _m3 : _m1); \
			})

#ifndef HAVE_BSWAP_64
#define	BSWAP_64(x)	(((uint64_t)(x) << 56) | \
			(((uint64_t)(x) << 40) & 0xff000000000000ULL) | \
			(((uint64_t)(x) << 24) & 0xff0000000000ULL) | \
			(((uint64_t)(x) << 8)  & 0xff00000000ULL) | \
			(((uint64_t)(x) >> 8)  & 0xff000000ULL) | \
			(((uint64_t)(x) >> 24) & 0xff0000ULL) | \
			(((uint64_t)(x) >> 40) & 0xff00ULL) | \
			((uint64_t)(x)  >> 56))
#endif

#ifndef HAVE_BSWAP_32
#define	BSWAP_32(x)	(((uint32_t)(x) << 24) | \
			(((uint32_t)(x) << 8)  & 0xff0000) | \
			(((uint32_t)(x) >> 8)  & 0xff00) | \
			((uint32_t)(x)  >> 24))
#endif

#ifndef HAVE_BSWAP_16
#define	BSWAP_16(x)	((((uint16_t)(x)) >> 8) | \
			(((uint16_t)(x))  << 8))
#endif

#define __M_BYTE_ORDER(size, from, to, how)				\
static inline uint##size##_t from##size##_##to(uint##size##_t x)	\
{									\
	return how;							\
}

#ifdef CPU_BIG_ENDIAN
#define M_BYTE_ORDER(size, endian, bhow, lhow) \
	__M_BYTE_ORDER(size, cpu, endian, bhow) \
	__M_BYTE_ORDER(size, endian, cpu, bhow)
#else
#define M_BYTE_ORDER(size, endian, bhow, lhow) \
	__M_BYTE_ORDER(size, cpu, endian, lhow) \
	__M_BYTE_ORDER(size, endian, cpu, lhow)
#endif

/* CPU <-> big endian */
M_BYTE_ORDER(64, be, x, BSWAP_64(x))
M_BYTE_ORDER(32, be, x, BSWAP_32(x))
M_BYTE_ORDER(16, be, x, BSWAP_16(x))
M_BYTE_ORDER(8, be, x, x)

/* CPU <-> little endian */
M_BYTE_ORDER(64, le, BSWAP_64(x), x)
M_BYTE_ORDER(32, le, BSWAP_32(x), x)
M_BYTE_ORDER(16, le, BSWAP_16(x), x)
M_BYTE_ORDER(8, le, x, x)

#undef M_BYTE_ORDER
#undef __M_BYTE_ORDER

#define ARRAY_LEN(x)	(sizeof(x) / sizeof(x[0]))

#define M_STR_TO_INT(size, imax)					\
static inline int __str2u##size(const char *restrict s,			\
				uint##size##_t *i,			\
				int base)				\
{									\
	char *endptr;							\
	uint64_t tmp;							\
									\
	*i = 0;								\
									\
	errno = 0;							\
	tmp = strtoull(s, &endptr, base);				\
									\
	if (errno)							\
		return -errno;						\
									\
	if (endptr == s)						\
		return -EINVAL;						\
									\
	if (tmp > imax)							\
		return -ERANGE;						\
									\
	*i = tmp;							\
	return 0;							\
}

M_STR_TO_INT(16, 0x000000000000fffful)
M_STR_TO_INT(32, 0x00000000fffffffful)
M_STR_TO_INT(64, 0xfffffffffffffffful)

#undef M_STR_TO_INT

#define str2u64(s, i)	__str2u64((s), (i), 10)
#define str2u32(s, i)	__str2u32((s), (i), 10)
#define str2u16(s, i)	__str2u16((s), (i), 10)

#endif
