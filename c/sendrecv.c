#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "error.h"
#include "sock.h"
#include "types.h"

uint64_t send_bytes;
uint64_t recv_bytes;
uint32_t sends;
uint32_t recvs;

static int __sendbuf(int fd, void *buf, size_t len)
{
	uint8_t *ptr = buf;
	int ret;

	if (!len)
		return 0;

	while (len) {
		ret = send(fd, ptr, len, 0);
		if (ret < 0) {
			if (errno == EINTR)
				continue;
			return -errno;
		}
		if (!ret)
			break;

		len -= ret;
		ptr += ret;
	}

	if (len)
		return -EIO;
	return 0;
}

static int __recvbuf(int fd, void *buf, size_t len)
{
	uint8_t *ptr = buf;
	int ret;

	if (!len)
		return 0;

	while (len) {
		ret = recv(fd, ptr, len, MSG_WAITALL);
		if (ret < 0) {
			if (errno == EINTR)
				continue;
			return -errno;
		}
		if (!ret)
			break;

		len -= ret;
		ptr += ret;
	}

	if (len)
		return -EIO;
	return 0;
}

int sendbuf(int fd, void *buf, size_t len)
{
	uint32_t tmp;
	int ret;

	tmp = cpu32_be(len);

	ret = __sendbuf(fd, &tmp, sizeof(tmp));
	if (ret)
		return ret;

	ret = __sendbuf(fd, buf, len);
	if (ret)
		return ret;

	send_bytes += len;
	sends++;

	return 0;
}

void *recvbuf(int fd, size_t *len)
{
	uint32_t tmp;
	char *buf;
	int ret;

	ret = __recvbuf(fd, &tmp, sizeof(tmp));
	if (ret)
		return ERR_PTR(ret);

	tmp = be32_cpu(tmp);

	buf = malloc(tmp + 1);
	if (!buf)
		return ERR_PTR(-ENOMEM);

	ret = __recvbuf(fd, buf, tmp);
	if (ret) {
		free(buf);
		return ERR_PTR(ret);
	}

	buf[tmp] = '\0';

	if (len)
		*len = tmp;

	recv_bytes += tmp;
	recvs++;

	return buf;
}
