#include <assert.h>

#include <yajl/yajl_tree.h>

#include "sock.h"
#include "state.h"
#include "rpc.h"
#include "error.h"
#include "agent.h"

enum pdu_type {
	PDU_GREETINGS = 1,
	PDU_RESULT,
	PDU_ERROR,
	PDU_REQUEST,
	PDU_MOVE,
    PDU_MOVERESULT,
	PDU_GAME_OVER,
    PDU_UNKNOWN,
};

struct pdu {
	int fd;

	char *raw;

	enum pdu_type type;
};

static enum pdu_type str2pdutype(const char *type)
{
	if (!strcmp(type, "greetings_program"))
		return PDU_GREETINGS;
	else if (!strcmp(type, "error"))
		return PDU_ERROR;
	else if (!strcmp(type, "request"))
		return PDU_REQUEST;
	else if (!strcmp(type, "move"))
		return PDU_MOVE;
    else if (!strcmp(type, "move_result"))
        return PDU_MOVERESULT;
	else if (!strcmp(type, "result"))
		return PDU_RESULT;
	else if (!strcmp(type, "game_over"))
		return PDU_GAME_OVER;

	fprintf(stderr, "Unknown PDU type encounted: '%s'\n", type);
	return PDU_UNKNOWN;
}

static uint64_t tree_int_default(yajl_val tree, const char **path, uint64_t def)
{
	yajl_val v;

	v = yajl_tree_get(tree, path, yajl_t_number);
	if (v)
		return YAJL_GET_INTEGER(v);

	return def;
}

static uint64_t tree_int(yajl_val tree, const char **path)
{
	yajl_val v;

	v = yajl_tree_get(tree, path, yajl_t_number);
    if (!v)
        fprintf(stderr, "Failed to find: %s\n", path[1]);
	assert(v);

	return YAJL_GET_INTEGER(v);
}

static bool tree_bool(yajl_val tree, const char **path)
{
	yajl_val v;

	v = yajl_tree_get(tree, path, yajl_t_any);
	assert(v);

	if (YAJL_IS_TRUE(v))
		return true;
	if (YAJL_IS_FALSE(v))
		return false;

	assert(0);
	return false;
}

static int __parse_board(struct state *state, yajl_val tree)
{
	static const char *board[] = { "state", "board", NULL, };

	yajl_val boardobj;
	uint32_t x, y, z;

	boardobj = yajl_tree_get(tree, board, yajl_t_array);
	assert(boardobj);
    assert(YAJL_IS_ARRAY(boardobj));

    state_board_init(state, boardobj->u.array.len);

    uint32_t size = state->board_characteristic_size;

	for (x = 0; x < size; x++) {
		assert(YAJL_IS_ARRAY(boardobj->u.array.values[x]));
        assert(boardobj->u.array.values[x]->u.array.len == (size - x));

        for (y = 0; y < (size - x); y++) {
            assert(YAJL_IS_ARRAY(boardobj->u.array.values[x]->u.array.values[y]));
            assert(boardobj->u.array.values[x]->u.array.values[y]->u.array.len == (size - x - y));

            for (z = 0; z < (size - x - y); z++) {
                assert(YAJL_IS_INTEGER(boardobj->u.array.values[x]->u.array.values[y]->u.array.values[z]));
                state->board[x][y][z] = YAJL_GET_INTEGER(boardobj->u.array.values[x]->u.array.values[y]->u.array.values[z]);
            }
        }
	}

	return 0;
}

static int __parse_legal_moves(struct state *state, yajl_val tree)
{
	static const char *legal_moves[] = { "state", "legal_moves", NULL, };

    yajl_val legalobj;
    uint32_t i;

    legalobj = yajl_tree_get(tree, legal_moves, yajl_t_array);

    /* legal_moves isn't always present, and that's okay (i.e., for move result messages) */
    if (legalobj == NULL)
        return 0;

    state_legal_moves_init(state, legalobj->u.array.len);
    
    for (i = 0; i < state->nlegal_moves; i++) {
        /* Each legal move should be an array of three integers (x, y, and z) */
        assert(legalobj->u.array.values[i] && YAJL_IS_ARRAY(legalobj->u.array.values[i]));
        assert(legalobj->u.array.values[i]->u.array.len == 3);

        int j;
        for (j = 0; j < 3; j++) {
            assert(YAJL_IS_INTEGER(legalobj->u.array.values[i]->u.array.values[j]));
            state->legal_moves[i][j] = YAJL_GET_INTEGER(legalobj->u.array.values[i]->u.array.values[j]);
        }
    }

    return 0;
}

static int __parse_state(struct state *state, yajl_val tree)
{
	static const char *opponent_id[] = { "state", "opponent_id", NULL, };
	static const char *game_id[] = { "game_id", NULL, };
    static const char *time_remaining[] = { "state", "time_remaining_ns", NULL, };
    static const char *moves_remaining[] = { "state", "moves_remaining", NULL, };
    static const char *tokens[] = { "state", "tokens", NULL, };
    static const char *opponent_tokens[] = { "state", "opponent_tokens", NULL, };
    static const char *player[] = { "state", "player", NULL, };
    static const char *player1_score[] = { "state", "score", "player1", NULL, };
    static const char *player2_score[] = { "state", "score", "player2", NULL, };

	state->gameid = tree_int(tree, game_id);
    if (YAJL_IS_INTEGER(yajl_tree_get(tree, opponent_id, yajl_t_number)))
        state->opponentid = tree_int(tree, opponent_id);
    if (YAJL_IS_INTEGER(yajl_tree_get(tree, player, yajl_t_number)))
        state->playerid = tree_int(tree, player);

	state->my_points  = tree_int(tree, state->playerid == 1 ? player1_score : player2_score);
	state->opp_points = tree_int(tree, state->playerid == 2 ? player1_score : player2_score);

    if (YAJL_IS_INTEGER(yajl_tree_get(tree, tokens, yajl_t_number)))
        state->my_tokens = tree_int(tree, tokens);

    if (YAJL_IS_INTEGER(yajl_tree_get(tree, opponent_tokens, yajl_t_number)))
        state->opp_tokens = tree_int(tree, opponent_tokens);

    state->time_remaining_ns = tree_int_default(tree, time_remaining, 0);
    state->moves_remaining = tree_int_default(tree, moves_remaining, 0);

	return __parse_legal_moves(state, tree) || __parse_board(state, tree);
}

static int __dispatch_request(struct state *state,
			      struct pdu *pdu, yajl_val tree)
{
	static const char *req_id_path[] = { "id", NULL, };

	char buf[1024];
	char out[1024];
	uint32_t req_id;
	int ret;

	/*
	 * parse
	 */

	req_id = tree_int(tree, req_id_path);

	ret = __parse_state(state, tree);
	if (ret)
		return ret;

	/*
	 * process
	 */

    struct move move;
    move_reset(&move);

    agent_request_move(state, &move);
    if (move.wait) {
        snprintf(buf, sizeof(buf), "\"wait\":true");
    }
    else {
        snprintf(buf, sizeof(buf), "\"location\":[%u,%u,%u]", move.x, move.y, move.z);
    }

	snprintf(out, sizeof(out),
		 "{"
			"\"type\":\"move\","
			"\"id\":%u,"
			"%s"
		 "}", req_id, buf);

	ret = sendbuf(pdu->fd, out, strlen(out));
	if (ret)
		fprintf(stderr, "Error sending response: %s\n",
			mstrerror(ret));

	return ret;
}

static enum result __get_result_winner(yajl_val tree, uint32_t me)
{
	static const char *by_path[] = { "result", "by", NULL, };

	int res;

	res = tree_int_default(tree, by_path, -1);

	if (res == -1)
		return RESULT_TIE;
	if (res == me)
		return RESULT_WIN;
	return RESULT_LOSS;
}

static int __dispatch_result(struct state *state,
			     struct pdu *pdu, yajl_val tree)
{
    static const char *claimed_path[] = { "state", "claimed", NULL };
    static const char *winner_path[] = { "state", "winner", NULL };
    int ret;

    ret = __parse_state(state, tree);
    if (ret)
        return ret;

    struct move claimed;
    yajl_val v;
    int winner;

    switch (pdu->type) {
        default:
            fprintf(stderr, "Unsupported result PDU type: %d\n", pdu->type);
            return -ENOTSUP;
        case PDU_MOVERESULT:
            v = yajl_tree_get(tree, claimed_path, yajl_t_array);
            if (v) {
                assert(v->u.array.len == 3);
                move_reset(&claimed);
                claimed.x = YAJL_GET_INTEGER(v->u.array.values[0]);
                claimed.y = YAJL_GET_INTEGER(v->u.array.values[1]);
                claimed.z = YAJL_GET_INTEGER(v->u.array.values[2]);

                agent_move_result(state, &claimed);
            }
            else
                agent_move_result(state, NULL);
            break;
        case PDU_GAME_OVER:
            winner = tree_int(tree, winner_path);
            agent_game_result(state, winner);
            break;
    }

	return 0;
}

static int __decode_and_dispatch(struct state *state,
				 struct pdu *pdu, yajl_val tree)
{
	int ret = -ENOTSUP;

	switch (pdu->type) {
        case PDU_UNKNOWN:
		default:
			fprintf(stderr, "Unsupported PDU type: %d\n",
				pdu->type);
			fprintf(stderr, "RAW JSON: '%s'\n", pdu->raw);
			ret = -ENOTSUP;
			break;
		case PDU_GREETINGS:
			ret = 0;
			break;
		case PDU_ERROR:
			fprintf(stderr, "\x1b[1;31mERROR PDU ENCOUNTERED!!!!!!!\n");
			fprintf(stderr, "RAW JSON: '%s'\n", pdu->raw);
			fprintf(stderr, "ERROR PDU ENCOUNTERED!!!!!!!\x1b[m\n");
			ret = -ENOSYS;
			break;
		case PDU_MOVERESULT:
        case PDU_GAME_OVER:
			ret = __dispatch_result(state, pdu, tree);
			break;
		case PDU_REQUEST:
			ret = __dispatch_request(state, pdu, tree);
			break;
	}

	/*
	 * If for whatever reason we could not process the PDU, let's dump
	 * the raw JSON.
	 */
	if (ret)
		fprintf(stderr, "RAW JSON: '%s'\n", pdu->raw);

	return ret;
}

static int decode_and_dispatch(struct state *state, struct pdu *pdu)
{
	static const char *type_path[] = { "type", NULL, };

	char errbuf[1024];
	yajl_val node;
	yajl_val v;
	int ret;

	ret = -EINVAL;

	node = yajl_tree_parse(pdu->raw, errbuf, sizeof(errbuf));
	if (!node) {
		fprintf(stderr, "PDU contains invalid JSON\n");
		goto err;
	}

	errbuf[0] = '\0';

	v = yajl_tree_get(node, type_path, yajl_t_string);
	if (!v) {
		fprintf(stderr, "PDU lacks 'type' entry\n");
		goto err;
	}

	pdu->type = str2pdutype(YAJL_GET_STRING(v));

	ret = __decode_and_dispatch(state, pdu, node);
	if (ret)
		goto err;

	yajl_tree_free(node);

	return 0;

err:
	if (node)
		yajl_tree_free(node);

	fprintf(stderr, " JSON:  '%s'\n", pdu->raw);
	if (strlen(errbuf))
		fprintf(stderr, " ERROR: %s\n", errbuf);
	else
		fprintf(stderr, " UNKNOWN ERROR\n");

	return ret;
}

int process_request(int fd, struct state *state)
{
	struct pdu pdu;
	char *buf;
	int ret;

	pdu.raw = NULL;

	buf = recvbuf(fd, NULL);
	if (IS_ERR(buf))
		return PTR_ERR(buf);

	pdu.fd = fd;
	pdu.raw = buf;

	ret = decode_and_dispatch(state, &pdu);

	free(buf);

	return ret;
}
