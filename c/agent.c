#include "agent.h"

void agent_request_move(struct state *state, struct move *move)
{
    int i;

    if (rand() % 2) {
        move->wait = false;

        i = rand() % state->nlegal_moves;
        move->x = state->legal_moves[i][0];
        move->y = state->legal_moves[i][1];
        move->z = state->legal_moves[i][2];
    }
    else {
        move->wait = true;
    }
}

void agent_move_result(struct state *state, struct move *claimed)
{
}

void agent_game_result(struct state *state, int winner)
{
    /* winner is the number of the winning player (1 or 2), or 0 for draws */
    fprintf(stderr, "Game over: winner = %u\n", winner);
}

void move_reset(struct move *move)
{
    memset(move, 0, sizeof(struct move));
}
