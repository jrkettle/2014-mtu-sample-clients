#ifndef __MOEBIUS_TIME_H
#define __MOEBIUS_TIME_H

#include <time.h>
#include <inttypes.h>

static inline uint64_t gettime()
{
	struct timespec ts;

	clock_gettime(CLOCK_REALTIME, &ts);

	return (ts.tv_sec * 1000000000ull) + ts.tv_nsec;
}

#endif
