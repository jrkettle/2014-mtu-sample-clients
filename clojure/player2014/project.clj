(defproject player2014 "0.1.0-SNAPSHOT"
  :description "Sample clojure bot for 2014 Barracuda Networks Programming Contest"
  :url "http://www.barracuda.com/umich"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/data.json "0.2.5"]]
  :main ^:skip-aot player2014.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[cider/cider-nrepl "0.7.0"]]}})
