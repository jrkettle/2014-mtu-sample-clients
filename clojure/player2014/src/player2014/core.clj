;; example clojure bot for 2014 Barracuda Networks, Inc. programming contest

(ns player2014.core
  (:gen-class)
  (:require [clojure.data.json :as json]))

(def contest-server "cuda.contest")
(def contest-port 9999)
(def reconnect-timeout 15) ; seconds
(def game-id -1)

(defn player-response [id msg]
  (assoc msg :id id :type "move"))

(defn player-wait-message [id]
  (player-response id {:wait true}))

(defn player-move-message [id coords]
  (player-response id {:location coords}))

(defn select-move [id msg]
  (if (< 0.5 (Math/random))
    (player-wait-message id)
    (player-move-message id (rand-nth (get-in msg [:state :legal_moves])))))

(defn handle-message [msg]
  ;(println "server request:" msg)
  (let [type (msg :type)
        id (msg :id)]
    (condp = type
      "greetings_program" nil
      "request" (do (when (not (= game-id (msg :game_id)))
                      (def game-id (msg :game_id))
                      (println "new game" game-id))
                    (select-move id msg))
      "move_result" nil
      "game_over" nil
      "error" (do (println "Error:" (get-in msg [:state :error]))
                  (when (contains? (msg :state) :seen_host)
                    (System/exit 1)))
      nil)))

(defn get-message [reader]
  (try
    (let [datalen (.readInt reader)
          json-in (byte-array datalen)
          num-bytes-read (.read reader json-in)]
      (if (< num-bytes-read 1)
        (throw (Exception. "server response truncated"))
        (json/read-str (String. json-in)
                       :key-fn keyword)))
    (catch Exception e
      (throw (Exception. (str "Error reading response from server:" e))))))

(defn send-message [writer response]
  (let [json (json/write-str response)
        json-out (.getBytes json "US-ASCII")]
    (.writeInt writer (count json-out))
    (.write writer json-out)
    (.flush writer)))

(defn game-loop []
  (with-open [sock (java.net.Socket. contest-server contest-port)
              reader (java.io.DataInputStream. (.getInputStream sock))
              writer (java.io.DataOutputStream. (.getOutputStream sock))]
    (.setTcpNoDelay sock true)
    (while true
      (let [response (handle-message (get-message reader))]
        (when response
          ;(println "response" response)
          (send-message writer response))))))

(defn -main
  "Connect to contest server, process requests, send responses, forever..."
  [& args]
  (while true
    (try
      (game-loop)
      (catch Exception e
        (println e)
        (println "Reconnecting in" reconnect-timeout "s")
        (Thread/sleep (* reconnect-timeout 1000))))))
