;;; player.el --- sample bot for Barracuda Networks 2014 programming contest

;; Copyright (C) 2014 Barracuda Networks, Inc. All rights reserved.

;; This file is not part of GNU Emacs.

;; Call `contest-bot-start' and `contest-bot-stop' to start/stop a bot

(require 'bindat)

;; https://github.com/thorstadt/json.el
(require 'json)

;; start of next message from server
(defvar contest-read-point nil)
(defvar contest-len-spec '((length u32)))
(defvar contest-msg-spec '((length u32)
                           (json str (length))))
(setq contest-game-id -1)

(defun contest-bot-start (host port)
  "Sample bot for Barracuda Networks 2014 programming contest.
Connect to server running at HOST:PORT.
This routine currently runs in the foreground, so you must
cancel with C-g. Afterwards, to kill the network connection,
call \(contest-bot-stop).
"
  (interactive "sHOST: \nnPORT: ")
  (random t)
  (let ((proc (open-network-stream "CONTEST" "*contest-bot-output*" host port))
        response)
    (set-process-coding-system proc 'binary 'binary)
    (save-excursion
      ;; initialize buffer for collecting server output
      (set-buffer (process-buffer proc))
      (erase-buffer)
      (set-buffer-multibyte nil)
      (setq contest-read-point (point-min))
      (catch 'break
        (while t
          (setq response
                (contest-handle-message proc (contest-read-server-message proc)))
          (when response
            (contest-send-response proc response)))))))

(defun contest-read-server-message (proc)
  (save-excursion
    (let (msg-start msg-end decoded len json msg)
      (set-buffer (process-buffer proc))
      (goto-char contest-read-point)
      ;; wait for message from server
      (while (and (memq (process-status proc) '(open run))
                  (not (search-forward "{" nil t))
                  (< (point-max) (+ 4 (point))))
        (accept-process-output proc 0.25)
        (goto-char contest-read-point)
        (sit-for 0.25))
      ;; get message length
      (goto-char contest-read-point)
      (setq msg-start (+ (point) 4))
      (setq decoded (bindat-unpack
                     contest-len-spec
                     (buffer-substring-no-properties (point) msg-start)))
      (setq len (bindat-get-field decoded 'length))
      (setq msg-end (+ msg-start len))
      (setq contest-read-point msg-end)
      ;; json payload
      (setq msg (json-read-from-string (buffer-substring msg-start msg-end)))
      ;;(contest-log proc msg)
      msg)))

(defun contest-send-response (proc response)
  (let ((json (json-encode response)))
    (process-send-string proc (bindat-pack contest-msg-spec
                                           `((length . ,(length json))
                                             (json . ,json))))))

(defun contest-log (proc s)
  "Emit a message to the *contest-bot-debug* buffer. PROC is the
connected network process and S is the string to emit.
"
  (save-excursion
    (switch-to-buffer (get-buffer-create "*contest-bot-debug*"))
    (goto-char (point-max))
    (insert (current-time-string) " " (prin1-to-string s) "\n")))

(defun contest-handle-message (proc msg)
  (let ((msgtype (cdr (assoc 'type msg))))
    (cond
     ((string-equal msgtype 'request)
      (when (/= contest-game-id (cdr (assoc 'game_id msg)))
        (setq contest-game-id (cdr (assoc 'game_id msg)))
        (contest-log proc (concat "new game " (number-to-string contest-game-id))))
      (if (= (random 2) 0)
          (contest-wait-response msg)
        (let ((legal_moves (cdr (assoc 'legal_moves (assoc 'state msg)))))
          (contest-move-response msg (elt legal_moves (random (length legal_moves)))))))
     ((string-equal msgtype 'error)
      (contest-log proc (cdr (assoc 'error (assoc 'state msg))))
      ;; need to register IP
      (if (assoc 'seen_host (assoc 'state msg))
          (progn
            (contest-bot-stop)
            (throw 'break nil))
        nil))
     ((string-equal msgtype 'move_result)
      ;; ...
      ;;(contest-log proc msg)
      nil)
     ((string-equal msgtype 'game_over)
      ;; ...
      ;;(contest-log proc msg)
      nil)
     ((string-equal msgtype 'greetings_program)
      (contest-log proc msg)
      nil))))

(defun contest-wait-response (msg)
  (contest-response msg `(wait . ,t)))

(defun contest-move-response (msg location)
  (contest-response msg `(location . ,location)))

(defun contest-response (msg details)
  `((type . move)
    (id . ,(contest-request-id msg))
    ,details))

(defun contest-request-id (msg)
  (cdr (assoc 'id msg)))

(defun contest-bot-stop (&optional proc)
  "Stop the running contest bot.
If PROC is specified, the bot connected to that process
will be terminated. Otherwise, the default \"CONTEST\"
process will be terminated"
  (interactive)
  (let ((p (or proc "CONTEST")))
    (when (eq 'open (process-status p)) (delete-process p))
    nil))


;;(setq contest-host "cuda.contest"
;;      contest-port 9999)
;;(contest-bot-start contest-host contest-port)
;;(contest-bot-stop)
