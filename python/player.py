#!/usr/bin/python

import argparse
import json
import socket
import struct
import sys
import time
import traceback
import random

parser = argparse.ArgumentParser(
       description="Sample bot for the Barracuda Networks programming contest.")
parser.add_argument("--server", type=str, default="cuda.contest",
                    help="server to connect to (default cuda.contest)")
parser.add_argument("--port", type=int, default=9999, help="port to connect to (default 9999)")
parser.add_argument("--debug", type=bool, default=False, help="whether to dump out all messages to the screen")

def sample_bot(host, port):
    s = SocketLayer(host, port)

    gameId = None

    while True:
        msg = s.pump()
        if args.debug:
            print msg, "\n"

        if msg["type"] == "error":
            print("Error: " + msg["state"]["error"])
            if "seen_host" in msg["state"]:
                print("The server doesn't know your IP. It saw: " + msg["state"]["seen_host"])
                sys.exit(1)
        elif msg["type"] == "request":
            msg = {
                "type": "move",
                "id": msg["id"],
                "location": msg["state"]["legal_moves"][0]
            }
            if args.debug:
                print "Sending:", msg, "\n"
            s.send(msg)
        elif msg["type"] == "greetings_program":
            print("Connected to the server.")
        elif msg["type"] == "move_result":
            pass
        elif msg["type"] == "game_over":
            pass

def loop(player, *args):
    while True:
        try:
            player(*args)
        except KeyboardInterrupt:
            sys.exit(0)
        except:
            traceback.print_exc()
        time.sleep(10)

class SocketLayer:
    def __init__(self, host, port):
        self.s = socket.socket()
        self.s.connect((host, port))

    def pump(self):
        """Gets the next message from the socket."""
        sizebytes = self.s.recv(4)
        (size,) = struct.unpack("!L", sizebytes)

        msg = []
        bytesToGet = size
        while bytesToGet > 0:
            b = self.s.recv(bytesToGet)
            bytesToGet -= len(b)
            msg.append(b)

        msg = "".join([chunk.decode('utf-8') for chunk in msg])

        return json.loads(msg)

    def send(self, obj):
        """Send a JSON message down the socket."""
        b = json.dumps(obj)
        length = struct.pack("!L", len(b))
        self.s.send(length + b.encode('utf-8'))

    def raw_send(self, data):
        self.s.send(data)

if __name__ == "__main__":
    args = parser.parse_args()
    loop(sample_bot, args.server, args.port)
