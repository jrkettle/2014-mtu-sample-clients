<?php

// include init to set autoloader
include_once('lib/init.php');

// number of games to play
$number_of_games = 1000000000;

// create Bot object
$Bot = new Bot('cuda.contest');

// enable debug logging
$Bot->Debug(false);
$Bot->DebugNetwork(false);

// connect to game server
$Bot->Connect();

// play games
for ($i = 0; $i < $number_of_games; $i++)
{
    // start the game and wait for a state
    $state = null;
    while ($state === null)
    {
        try
        {
            $state = $Bot->Start();
        }
        catch (Exception $e)
        {
            // log exception
            $Bot->Log('Exception: (' . $e->getCode() . ') ' . $e->getMessage(), LOG_LEVEL_ERROR);

            // clear state
            $state = null;

            // wait, don't thrash
            sleep(1);
        }
    }

    // play until the game is completed
    while (! $Bot->game_completed)
    {
        if (mt_rand(0,10) > 5)
        {
            $state = $Bot->Wait();
        }
        else
        {
            $state = $Bot->TakeLocation($state['legal_moves'][0]);
        }
    }

    // show game result
    $result = $Bot->GameResult();
}

// stop game play
$Bot->Stop();

// destruct Bot
unset($Bot);

exit(0);
