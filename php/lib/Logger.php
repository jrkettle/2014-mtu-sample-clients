<?php

define('LOG_LEVEL_INFO', 'INFO');
define('LOG_LEVEL_WARNING', 'WARNING');
define('LOG_LEVEL_ERROR', 'ERROR');
define('LOG_LEVEL_DEBUG', 'DEBUG');

trait Logger
{
    private $Logger_file = null;
    private $Logger_stdout = true;
    private $Logger_debug = false;

    public function Log($message, $level = LOG_LEVEL_INFO)
    {
        // shortcut not logging debug if not enabled
        if ((! $this->Logger_debug) && ($level == LOG_LEVEL_DEBUG))
            return true;

        // get backtrace to log the caller of Log
        $backtrace = debug_backtrace();
        if (array_key_exists('1', $backtrace))
        {
            $backtrace = $backtrace[1];
        }
        else
        {
            $backtrace = $backtrace[0];
        }

        // format log message
        $message = date('Y-m-d H:i:s') . ' (' . $level . ') [' . (! isset($backtrace['class']) ?: $backtrace['class'] . '::') . $backtrace['function'] . '] ' . $message . PHP_EOL;

        // log to stdout if desired
        if ($this->Logger_stdout)
            print $message;

        // log to file if desired
        if (! empty($this->Logger_file))
            file_put_contents($this->Logger_file, $message, FILE_APPEND);

        // memory cleanup
        unset($backtrace, $message);

        return true;
    }

    public function LogFile($file = null)
    {
        $this->Logger_file = $file;
    }

    public function LogStdout($value = true)
    {
        $this->Logger_stdout = (bool) $value;
    }

    public function LogDebug($value = true)
    {
        $this->Logger_debug = (bool) $value;
    }
}

