<?php

/**
 * This is a class which extends network to allow for
 * communication with Contest Bot Server
 *
 * All methods of communication return or accept array of data.
 * Simple validation is performed to avoid sending garbage to
 * Contest Bot Server
 * All functions are documented as needed.  Errors result in
 * an Exception being thrown, handle them accordingly.
 */

class BotNetwork extends Network
{
    // define used traits
    use Logger;

    // class constants
    const HEADER_LENGTH = 4;

    /**
     * Class constructor
     *
     * @param string $host hostname or ip address to connect to
     * @param integer $port port to connect to on host
     * @param integer $connection_timeout connection timeout in seconds
     * @param integer $read_timeout connection read timeout in seconds
     */
    public function __construct($host, $port = CONTEST_SERVER_PORT, $connection_timeout = 10, $read_timeout = 30)
    {
        // check that we have the json module loaded
        if (! extension_loaded('json'))
        {
            throw new BotNetworkException('Required PHP module json not loaded', BotNetworkException::NO_JSON);
        }

        // call parent constructor
        parent::__construct($host, $port, $connection_timeout, $read_timeout);
    }

    /**
     * Class destructor
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Receive data from the bot server
     *
     * @return array received bot data, Exception on error
     */
    public function read()
    {
        // read size header
        $size_packed = parent::receive(self::HEADER_LENGTH);

        // unpack header into integer
        if (($size = unpack('N', $size_packed)) === false)
        {
            throw new BotNetworkException('Failed to unpack packet header', BotNetworkException::READ_HEADER);
        }
        if (empty($size))
        {
            throw new BotNetworkException('Failed to unpack packet header', BotNetworkException::READ_HEADER);
        }
        $size = $size[1];

        // debug log
        $this->Log(__METHOD__ . ': reading ' . $size . ' bytes from socket', LOG_LEVEL_DEBUG);

        // read socket until we get all the expected data
        $data = '';
        while (strlen($data) < $size)
        {
            $data .= parent::receive($size - strlen($data));
        }

        // debug log
        $this->Log(__METHOD__ . ': read ' . $size . ' bytes with data: ' . var_export($data, true), LOG_LEVEL_DEBUG);

        // json decode the data portion of the bot packet
        if (($data = json_decode($data, true)) === false)
        {
            throw new BotNetworkException('Failed to JSON decode data: ' . var_export($data, true), BotNetworkException::DECODE);
        }

        // check for error from server and exception
        if (array_key_exists('type', $data) && ($data['type'] == 'error'))
        {
            if (array_key_exists('message', $data))
            {
                // server has disconnected us
                throw new ServerBotNetworkException('Server Error: ' . $data['message'], ServerBotNetworkException::SERVER_ERROR);
            }
            throw new ServerBotNetworkException('Server Error: No message provided', ServerBotNetworkException::SERVER_ERROR);
        }

        return $data;
    }

    /**
     * Send data to the bot server
     *
     * @param array $array data to send on socket
     * @return None, Exception on error
     */
    public function send($array)
    {
        // valid data within array
        $this->validate($array);

        // json encode the array
        if (($data = json_encode($array)) === false)
        {
            throw new BotNetworkException('Failed to JSON encode array: ' . var_export($array, true), BotNetworkException::ENCODE);
        }

        // create packet header
        $header = pack('N', strlen($data));

        // debug log
        $this->Log(__METHOD__ . ': send ' . strlen($data) . ' bytes with data: ' . var_export($data, true), LOG_LEVEL_DEBUG);

        // send the data
        parent::send($header . $data);
    }

    /**
     * Validate and enforce the array for data type correctness
     *
     * @param array $array array to be json encoded and sent to contest server
     * @return none, Exception on error
     */
    public function validate(&$array)
    {
        // validate input
        if (! is_array($array))
        {
            throw new DataBotNetworkException('Input must be an hash array', DataBotNetworkException::NOT_ARRAY);
        }

        // validate request id
        if (! array_key_exists('id', $array))
        {
            throw new DataBotNetworkException('Required index \'id\' missing', DataBotNetworkException::INVALID);
        }
        if ((! is_numeric($array['id'])) || ($array['id'] < 1))
        {
            throw new DataBotNetworkException('Invalid \'id\' value \'' . var_export($array['id'], true) . '\'', DataBotNetworkException::INVALID);
        }
        $array['id'] = (int) $array['id'];

        // validdate types
        switch ($array['type'])
        {
            case 'move':
                // validate response categories
                if (array_key_exists('wait', $array))
                {
                }
                elseif (array_key_exists('location', $array))
                {
                }
                else
                {
                    throw new DataBotNetworkException('Missing \'wait\' or \'location\' key for type \'move\'', DataBotNetworkException::INVALID);
                }
                break;

            default:
                throw new DataBotNetworkException('Invalid type: ' . $array['type'], DataBotNetworkException::INVALID);
        }
    }
}

/**
 * Bot Network class exception classes
 */
class BotNetworkException extends Exception
{
    const READ_HEADER = 400;
    const DECODE = 401;
    const SEND_HEADER = 402;
    const SEND_ENCODE = 403;
    const NO_JSON = 404;
}

class ServerBotNetworkException extends BotNetworkException
{
    const SERVER_ERROR = 500;
    const INVALID_INPUT = 501;
}

class DataBotNetworkException extends BotNetworkException
{
    const INVALID = 600;
    const NOT_ARRAY = 601;
}

