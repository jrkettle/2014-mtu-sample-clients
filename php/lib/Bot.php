<?php

class Bot
{
    // include traits
    use Logger;

    // class variable
    private $BotNetwork = null;
    public $request_id = null;
    public $state = null;
    public $response = null;
    public $result = null;
    public $team_id = null;
    public $tokens = 0;
    public $player_number = null;
    public $my_score = null;
    public $their_score = null;
    public $game_completed = false;

    public function __construct($host, $log_file = false, $port = CONTEST_SERVER_PORT)
    {
        // initialize Bot Network connection
        $this->BotNetwork = new BotNetwork($host, $port);

        // setup log file, false use default of Bot.log, null to disable, string of file path
        if ($log_file === false)
        {
            $log_file = __CLASS__ . '.log';
        }
        elseif (($log_file !== null) && (! is_string($log_file)))
        {
            // prevent invalid log file
            $log_file = null;
        }

        // set class logger
        $this->LogFile($log_file);

        // set log file for Bot Network
        $this->BotNetwork->LogFile($log_file);
    }

    public function __destruct()
    {
        $this->Stop();
    }

    public function Debug($value = true)
    {
        // set Bot logging debug level
        $this->LogDebug($value);
    }

    public function DebugNetwork($value = true)
    {
        // set Bot network logging debug level
        $this->BotNetwork->LogDebug($value);
    }

    private function server_response()
    {
        // read response from server
        $this->response = $this->BotNetwork->read();

        // retain team id
        if (array_key_exists('team_id', $this->response))
            $this->team_id = $this->response['team_id'];

        // retain request id for future Bot response
        if (array_key_exists('id', $this->response))
            $this->request_id = $this->response['id'];

        // store the state for use later
        if (array_key_exists('state', $this->response))
        {
            $this->state = $this->response['state'];
            if (array_key_exists('player', $this->response['state']))
            {
                $this->player_number = $this->response['state']['player'];
            }

            if (array_key_exists('score', $this->response['state']))
            {
                $my_player = "player".$this->player_number;
                $their_player = "player".($this->player_number == 1 ? 1 : 2);

                if (array_key_exists($my_player, $this->response['state']['score']))
                {
                    $this->my_score = $this->response['state']['score'][$my_player];
                }

                if (array_key_exists($their_player, $this->response['state']['score']))
                {
                    $this->their_score = $this->response['state']['score'][$their_player];
                }
            }

            if (array_key_exists('tokens', $this->response['state']))
            {
                $this->tokens = $this->response['state']['tokens'];
            }
        }

        // process response
        switch ($this->response['type'])
        {
            case 'greetings_program':
                // log welcome
                $this->Log('Welcome: ' . $this->response['state']['welcome']);
                return;

            case 'request':
                $this->Log('Incoming request', LOG_LEVEL_DEBUG);
                break;

            case 'move_result':
                // log result
                $res = '';
                if (array_key_exists('claimed', $this->response['state']))
                {
                    $res = "Claimed " . json_encode($this->response['state']['claimed']);
                }
                else if (array_key_exists('waited', $this->response['state']))
                {
                    $res = "Waited";
                }

                $this->Log('Result: ' . $res, LOG_LEVEL_DEBUG);

                break;
            case 'game_over':
                // mark game completed
                $this->game_completed = true;
                break;

            default:
                // log unknown response
                $this->Log('Unknown response encountered: ' . var_export($this->response, true), LOG_LEVEL_WARNING);
        }

        // return game state information
        return $this->state;
    }


    /**
     * Connect to game server and request game play
     *
     * @return None
     */
    public function Connect()
    {
        // debug logging
        $this->Log(__METHOD__, LOG_LEVEL_DEBUG);

        // get server welcome
        return $this->server_response();
    }

    /**
     * Start a game, must be called shortly after connect, otherwise timeout will occur
     *
     * @return array server response
     */
    public function Start()
    {
        // debug logging
        $this->Log(__METHOD__, LOG_LEVEL_DEBUG);

        // get game completed status
        $this->game_completed = false;

        // get first game state
        return $this->server_response();
    }

    /**
     * Wait (Get a token)
     *
     * @return array server response
     */
    public function Wait()
    {
        // debug logging
        $this->Log(__METHOD__, LOG_LEVEL_DEBUG);

        // create response
        $response = array(
            'id' => $this->request_id,
            'type' => 'move',
            'wait' => True
            );

        // Send response to server
        $this->BotNetwork->send($response);

        // read response
        $this->server_response();

        // Look for next request
        return $this->server_response();
    }

    /**
     * TakeLocation
     *
     * @return array server response
     */
    public function TakeLocation($location)
    {
        // debug logging
        $this->Log(__METHOD__ . ':' . json_encode($location), LOG_LEVEL_DEBUG);

        // create response
        $response = array(
            'id' => $this->request_id,
            'type' => 'move',
            'location' => $location
            );

        // Send response to server
        $this->BotNetwork->send($response);

        // read response
        $this->server_response();

        // Look for next request
        return $this->server_response();
    }

    /**
     * Logs game result and reset bot status, retains result in bot
     *
     * @return array game result array
     */
    public function GameResult()
    {
        // log result
        $result = 'LOSS';
        if ($this->state['winner'] === 0)
        {
            $result = 'DRAW';
        }
        else if ($this->player_number == $this->state['winner'])
        {
            $result = 'WIN';
        }

        $this->Log(__METHOD__ . ': ' . $result);

        // reset bot status
        $this->request_id = null;
        $this->state = null;
        $this->response = null;
        $this->team_id = null;
        $tokens = 0;
        $this->player_number = null;
        $my_score = null;
        $their_score = null;
        $this->game_completed = false;

        return $this->result;
    }

    /**
     * Stop playing and disconnect from game server
     *
     * @return None
     */
    public function Stop()
    {
        // debug logging
        $this->Log(__METHOD__, LOG_LEVEL_DEBUG);

        $this->BotNetwork->disconnect();
    }

}
