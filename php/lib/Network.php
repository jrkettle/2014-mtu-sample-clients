<?php

/**
 * This is a simple base network class that is used to
 * communicate with the Contest Bot Server.
 *
 * All functions are documented as needed.  Errors result in
 * an Exception being thrown, handle them accordingly.
 */

class Network
{
    // class variables
    private $host = null;
    private $port = null;
    private $connection_timeout = null;
    private $read_timeout = null;
    private $socket = null;

    /**
     * Class constructor
     *
     * @param string $host hostname or ip address to connect to
     * @param integer $port port to connect to on host
     * @param integer $connection_timeout connection timeout in seconds
     * @param integer $read_timeout connection read timeout in seconds
     */
    public function __construct($host, $port, $connection_timeout, $read_timeout)
    {
        // validate inputs
        if ((! is_numeric($port)) || ($port === 0))
        {
            throw new NetworkException('Invalid port: ' . $port, NetworkException::VALIDATION);
        }
        if ((! is_numeric($connection_timeout)) || ($connection_timeout === 0))
        {
            throw new NetworkException('Invalid connection timeout: ' . $connection_timeout, NetworkException::VALIDATION);
        }
        if ((! is_numeric($read_timeout)) || ($read_timeout === 0))
        {
            throw new NetworkException('Invalid read timeout: ' . $read_timeout, NetworkException::VALIDATION);
        }

        // set connection variables in class
        $this->host = $host;
        $this->port = $port;
        $this->connection_timeout = $connection_timeout;
        $this->read_timeout = $read_timeout;
    }

    /**
     * Class destructor
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Open socket connection, will be called on first action to perform connection
     *
     * @return None, Exception on error
     */
    public function connect()
    {
        // open socket to host if one is not already open
        if ($this->socket === null)
        {
            // open socket as file handle
            if (($this->socket = fsockopen($this->host, $this->port, $errno, $errstr, $this->connection_timeout)) === false)
            {
                // problem opening socket
                throw new ConnectionNetworkException('Socket open error (' . $errno . ') ' . $errstr, ConnectionNetworkException::SOCKET_OPEN);
            }

            // set socket read timeout
            if (stream_set_timeout($this->socket, $this->read_timeout) === false)
            {
                throw new ConnectionNetworkException('Failed to set receive timeout', ConnectionNetworkException::READ_TIMEOUT);
            }
        }
    }

    /**
     * Close socket
     */
    public function disconnect()
    {
        // close socket and reset socket, if needed
        if (is_resource($this->socket))
        {
            fclose($this->socket);
        }
        $this->socket = null;
    }

    /**
     * Receive data from connected socket, if not connected, it will be automatically
     *
     * @param integer $length length in bytes to read from socket
     * @return string received data, Exception on error
     */
    public function receive($length)
    {
        // connect if not already
        $this->connect();

        // read data from socket
        if (($data = fread($this->socket, $length)) === false)
        {
            throw new TransmissionNetworkException('Failed to read data from socket', TransmissionNetworkException::READ_ERROR);
        }

        // check for timeout/server disconnect
        if (strlen($data) != $length)
        {
            throw new ConnectionNetworkException('socket read timed out strlen ' .strlen($data)." l $length" , ConnectionNetworkException::READ_TIMEOUT);
        }

        return $data;
    }

    /**
     * Send data on connected socket, if not connected, it will be automatically
     *
     * @param string $data data to send on socket
     * @return None, Exception on error
     */
    public function send($data)
    {
        // connect if not already
        $this->connect();

        // send data over socket
        if (fwrite($this->socket, $data, strlen($data)) === false) // passing length to avoid magic quoting of string data
        {
            throw new TransmissionNetworkException('Failed to send data to socket', TransmissionNetworkException::SEND_ERROR);
        }
    }
}

/**
 * Network class exception classes
 */

class NetworkException extends Exception
{
    const VALIDATION = 100;
}

class ConnectionNetworkException extends NetworkException
{
    const SOCKET_OPEN = 200;
    const READ_TIMEOUT = 201;
}

class TransmissionNetworkException extends NetworkException
{
    const READ_ERROR = 300;
    const SEND_ERROR = 301;
}

