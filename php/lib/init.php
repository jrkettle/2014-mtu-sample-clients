<?php

// Global constants
define('CONTEST_SERVER_PORT', 9999);

/**
 * Class Autoloader
 *
 @param string $class_name class name
 @return None
 */
function __autoload($class_name)
{
    // generate base file name
    $filename = $class_name . '.php';

    // favor lib directory over working directory for autoloading classes
    include_once((file_exists('lib/' . $filename) ? 'lib/' . $filename : $filename));
}

