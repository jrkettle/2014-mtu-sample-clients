#!/usr/bin/env ruby

require 'rubygems'
require "socket"
require 'json'

class ContestBot
    attr_accessor :hostname, :port, :current_game_id

    def initialize
        @hostname = "cuda.contest"
        @port = 9999
        @current_game_id = 0
    end

    def run
        socket = TCPSocket.new @hostname, @port
        while true
            request = get_request(socket)
            response = handle_request(request)

            if response
                if !response[:id]
                  response[:id] = request[:id]
                end

                send_response(socket, response)
            end
        end
    end

    def get_request(socket)
        length = socket.recv(4).unpack('N')

        json = socket.recv(length[0])
        JSON.parse(json, symbolize_names: true)
    end

    def send_response(socket, response)
        response_json = JSON.generate(response)
        socket.send([response_json.length].pack('N'), 0)
        socket.send(response_json, 0)
    end

    # handle_request is called for every request from the game server.
    # It is passed a request object.
    def handle_request(request)
        case request[:type]
        when "connected"
            return
        when "request"
            if request[:game_id] != current_game_id
                current_game_id = request[:game_id]
                puts "New game: #{current_game_id}"
            end

            return {
                type: :move,
                location: request[:state][:legal_moves][0]
            }
        end
    end
end

bot = ContestBot.new

while true
    begin
        bot.run
    rescue Exception => e
        puts "Error connecting to #{bot.hostname}:#{bot.port}. Retrying in 10 seconds."
        puts e
        sleep 10
    end
end
