local socket = require("socket") -- http://files.luaforge.net/releases/luasocket/luasocket/luasocket-2.0.2
local json = require("json") -- https://github.com/luaforge/json/archive/master.zip
require("struct") -- http://www.inf.puc-rio.br/~roberto/struct/

host = "cuda.contest"
port = 9999

function decide(msg)

    local type = msg["type"]
    if type == "error" then
        for k,v in pairs(msg) do
            print(k.." "..v)
        end
        return
    end

    if type == "request" then
        local send = {type="move",location=msg["state"]["legal_moves"][1], id=msg["id"]}
        send_msg(send)
    end

end

function send_msg(msg)
    msg = json.encode(msg)
    local length = struct.pack(">i4", string.len(msg))
    local sent = con:send( length .. msg )
end

function recv_msg()
    local string = con:receive(4)
    if not string then
        print("Disconnected")
        os.exit()
    end
    local length = struct.unpack(">i4", string)
    local data = con:receive(length)
    local msg = json.decode(data, 0)
    return msg
end

print("Lua player started! Connecting to "..host..":"..port)
while 1 do
    con = socket.connect(host, port)
    if con then
        while 1 do
            local msg = recv_msg()
            decide(msg)
        end
    end
    socket.select(nil,nil,10)
end

