#!/usr/bin/env tclsh8.5
#
# example Tcl bot for 2014 Barracuda Networks, Inc. programming contest

package require json

set verbose 1
set host cuda.contest
set port 9999

# seconds
set reconnect_delay 15

set game_id -1

proc debug msg {
    global verbose
    if {$verbose == 1} {
        puts $msg
    }
}

proc run {host port} {
    global reconnect_delay
    while { 1 } {
        if {[catch {
            set sock [socket $host $port]
            fconfigure $sock -translation binary

            while { 1 } {
                set msg [getmessage $sock]

                set response [handlemessage $msg]
                if {$response ne ""} {
                    sendmessage $sock $response
                }
            }
        }] } {
            debug "Error: \"$::errorInfo\""
            debug "Reconnecting in $reconnect_delay seconds"
            after [expr {$reconnect_delay * 1000}]
        }
    }
}

proc getmessage sock {
    set len [read $sock 4]
    if {[string length $len] != 4} {
        close $sock
        return -code error "empty length read from server"
    }
    binary scan $len I readlen
    if {[catch {read $sock $readlen} buf]} {
        close $sock
        return -code error "failed to read payload from server"
    }

    return [json::json2dict $buf]
}

proc sendmessage {sock json} {
    set writelen [string length $json]
    if {[catch {
         puts -nonewline $sock [binary format I $writelen]
         puts -nonewline $sock $json
         flush $sock
    } ]} {
        close $sock
        return -code error 1 "failed to write to server"
    }
}

proc handlemessage msg {
    global game_id

    # Move request
    if {[dict get $msg type] eq "request"} {
        if {$game_id != [dict get $msg game_id]} {
            set game_id [dict get $msg game_id]
            debug "new game $game_id"
        }
        if {[expr {int(rand()*2)}] == 0} {
            return [move_response $msg "\"wait\":true"]
        } else {
            set legal_moves [dict get $msg state legal_moves]
            set size [llength $legal_moves]
            set idx [expr {int(rand()*$size)}]
            set point [lindex $legal_moves $idx]
            set x [lindex $point 0]
            set y [lindex $point 1]
            set z [lindex $point 2]
            return [move_response $msg "\"location\":\[$x,$y,$z\]"]
        }
    } elseif {[dict get $msg type] eq "move_result"} {
        # ...
    } elseif {[dict get $msg type] eq "game_over"} {
        # ...
    } elseif {[dict get $msg type] eq "greetings_program"} {
        # ...
    } elseif {[dict get $msg type] eq "error"} {
        debug "Error: [dict get $msg state error]"
        # need to register IP address
        if {[dict exists $msg state seen_host]} {
            exit 1
        }
    }
    return
}

proc move_response {msg details} {
    set id [dict get $msg id]
    set json "{\"type\":\"move\",\"id\":$id,$details}"
    return $json
}

if { $::argc > 0 } {
    set host [lindex $::argv 0]
}
if { $::argc > 1 } {
    set port [lindex $::argv 1]
}

debug "connecting to $host:$port"
run $host $port
