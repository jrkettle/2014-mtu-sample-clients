Object subclass: #ContestBot
	instanceVariableNames: 'gameId peerHost peerPort running'
	classVariableNames: 'ReconnectDelay'
	poolDictionaries: ''
	category: 'Barracuda'!
!ContestBot commentStamp: 'ez 9/13/2014 17:15' prior: 0!
Sample bot implementation. Update handleMessage with your game logic.!


!ContestBot methodsFor: 'initialize-release' stamp: 'ez 9/15/2014 01:57'!
host: aString port: anInteger 
	"Set contest host and port"
	peerHost := aString.
	peerPort := anInteger! !

!ContestBot methodsFor: 'initialize-release' stamp: 'ez 9/17/2014 12:43'!
initialize
	"seconds"
	ReconnectDelay := 15.
	gameId := -1.
	peerHost := 'cuda.contest'.
	peerPort := 9999.
	running := true! !


!ContestBot methodsFor: 'evaluating' stamp: 'ez 9/17/2014 12:45'!
run
	"Play the game forever. Reconnect on error after
	delay."
	| sock serverRequest myResponse |
	[sock := JSONSocket new host: peerHost port: peerPort.
	[running]
		whileTrue: [serverRequest := sock getRequest.
			self setGameId: serverRequest.
			myResponse := ContestBotMessage getResponseFor: serverRequest.
			"Transcript showln: 'my response is ' ,
			myResponse."
			myResponse isNil
				ifFalse: [sock sendResponse: myResponse]]]
		on: Exception
		do: [:e | 
			Transcript showln: e messageText.
			sock close.
			Transcript showln: 'Reconnecting in ' , ReconnectDelay , ' seconds'.
			(Delay forSeconds: ReconnectDelay) wait.
			e retry]! !

!ContestBot methodsFor: 'evaluating' stamp: 'ez 9/17/2014 12:42'!
stop
	"Stop processing."
	running := false! !


!ContestBot methodsFor: 'private' stamp: 'ez 9/16/2014 23:35'!
setGameId: serverRequest 
	"This should really go in the request handler"
	((serverRequest at: 'type')
				= 'request'
			and: [(serverRequest at: 'game_id')
					~= gameId])
		ifTrue: [gameId := serverRequest at: 'game_id'.
			Transcript showln: DateAndTime now asTimeStamp asString , ' new game ' , gameId]! !


Object subclass: #ContestBotMessage
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Barracuda'!
!ContestBotMessage commentStamp: 'ez 9/16/2014 01:07' prior: 0!
Base class for messages from server.!


!ContestBotMessage methodsFor: 'response generation' stamp: 'ez 9/16/2014 22:49'!
handleRequest: aCollection 
	"Default handler"
	"Transcript showln: 'server request: ' , aCollection."
	^ nil! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

ContestBotMessage class
	instanceVariableNames: ''!

!ContestBotMessage class methodsFor: 'as yet unclassified' stamp: 'ez 9/16/2014 17:30'!
getHandlerFor: aString 
	"Determine proper subclass to handle server
	request "
	| handlerClass |
	handlerClass := self subclasses
				detect: [:cls | cls type = aString]
				ifNone: [^ nil].
	^ handlerClass new! !

!ContestBotMessage class methodsFor: 'as yet unclassified' stamp: 'ez 9/16/2014 01:42'!
getResponseFor: aCollection 
	"Create an instance of the proper subclass to handle server request
	and invoke its handleRequest method."
	| handler |
	handler := self
				getHandlerFor: (aCollection at: 'type').
	handler isNil
		ifTrue: [^ nil]
		ifFalse: [^ handler handleRequest: aCollection]! !


ContestBotMessage subclass: #ContestBotMessageError
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Barracuda'!
!ContestBotMessageError commentStamp: 'ez 9/16/2014 00:47' prior: 0!
Error message from server.!


!ContestBotMessageError methodsFor: 'response generation' stamp: 'ez 9/16/2014 23:09'!
handleRequest: aCollection 
	"Log error text, force exit on seen_host error"
	| state errorMessage |
	state := aCollection at: 'state'.
	errorMessage := state at: 'error'.
	(state includesKey: 'seen_host')
		ifTrue: [self error: errorMessage , ' ('
					, (state at: 'seen_host') , ')']
		ifFalse: [Transcript showln: 'Error: ' , errorMessage].
	^ nil! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

ContestBotMessageError class
	instanceVariableNames: ''!

!ContestBotMessageError class methodsFor: 'as yet unclassified' stamp: 'ez 9/16/2014 01:19'!
type
	^ 'error'! !


ContestBotMessage subclass: #ContestBotMessageMoveResult
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Barracuda'!
!ContestBotMessageMoveResult commentStamp: 'ez 9/16/2014 00:48' prior: 0!
Move result from server.!


!ContestBotMessageMoveResult methodsFor: 'response generation' stamp: 'ez 9/16/2014 17:50'!
handleRequest: aCollection 
	"Log only..."
	"| state |
	state := aCollection at: 'state'.
	(state includesKey: 'claimed')
		ifTrue: [Transcript showln: 'Claimed point '
					, (state at: 'claimed')].
	^ nil"
	^ super handleRequest: aCollection! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

ContestBotMessageMoveResult class
	instanceVariableNames: ''!

!ContestBotMessageMoveResult class methodsFor: 'as yet unclassified' stamp: 'ez 9/16/2014 01:19'!
type
	^ 'move_result'! !


ContestBotMessage subclass: #ContestBotMessageRequest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Barracuda'!
!ContestBotMessageRequest commentStamp: 'ez 9/16/2014 00:47' prior: 0!
Move request from server.!


!ContestBotMessageRequest methodsFor: 'response generation' stamp: 'ez 9/16/2014 17:32'!
handleRequest: aCollection 
	"This is the core of the game logic. Given a move
	request from the server, generate a move or
	wait response"
	| response |
	response := Dictionary newFrom: {'type' -> 'move'. 'id'
					-> (aCollection at: 'id')}.
	Random new next < 0.5
		ifTrue: [response at: 'wait' put: true]
		ifFalse: [| legalMoves |
			legalMoves := (aCollection at: 'state')
						at: 'legal_moves'.
			response at: 'location' put: legalMoves atRandom].
	^ response! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

ContestBotMessageRequest class
	instanceVariableNames: ''!

!ContestBotMessageRequest class methodsFor: 'as yet unclassified' stamp: 'ez 9/16/2014 01:20'!
type
	^ 'request'! !


Object subclass: #JSONSocket
	instanceVariableNames: 'sock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Barracuda'!
!JSONSocket commentStamp: 'ez 9/13/2014 17:10' prior: 0!
Socket wrapper implementing the BC-Socket JSON protocol for sending/receiving messages.!


!JSONSocket methodsFor: 'initialize-release' stamp: 'ez 9/15/2014 01:56'!
close
	"Close connection to server"
	sock closeAndDestroy! !

!JSONSocket methodsFor: 'initialize-release' stamp: 'ez 9/15/2014 01:56'!
host: aString port: anInteger 
	"Establish a TCP connect to host:port"
	sock := Socket newTCP.
	sock connectToHostNamed: aString port: anInteger! !


!JSONSocket methodsFor: 'receiving' stamp: 'ez 9/17/2014 12:49'!
getRequest
	"Retrieve a single request from the server. Returns a Dictionary"
	| sizeBuf numBytesToRead payloadBuf |
	sizeBuf := ByteArray new: 4.
	sock receiveDataInto: sizeBuf.
	numBytesToRead := sizeBuf longAt: 1 bigEndian: true.
	numBytesToRead = 0
		ifTrue: [self error: 'EOF from server'].
	payloadBuf := ByteString new: numBytesToRead.
	sock receiveDataInto: payloadBuf.
	"Transcript showln: 'read json request ' , payloadBuf."
	^ Json readFrom: payloadBuf readStream! !


!JSONSocket methodsFor: 'sending' stamp: 'ez 9/16/2014 17:38'!
sendResponse: aDictionary 
	"Send response (aDictionary) to contest server"
	| sizeBuf numBytesToSend json |
	json := (Json render: aDictionary) asByteArray.
	numBytesToSend := json byteSize.
	sizeBuf := ByteArray new: 4.
	sizeBuf
		longAt: 1
		put: numBytesToSend
		bigEndian: true.
	sock sendData: sizeBuf.
	"Transcript showln: 'going to send json ' , json."
	sock sendData: json! !
