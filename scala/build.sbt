mainClass in (Compile,run) := Some("CudaBotMain")

scalaVersion := "2.11.2"

resolvers += "spray" at "http://repo.spray.io/"

libraryDependencies += "io.spray" % "spray-json_2.10" % "1.2.5"
