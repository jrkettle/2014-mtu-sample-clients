import spray.json._
import DefaultJsonProtocol._

object ContestBotJsonProtocol extends DefaultJsonProtocol {
  implicit val scoreFormat = jsonFormat2(Score)
  implicit val unknownPlayerErrorFormat = jsonFormat3(UnknownPlayerError)
  implicit val connectedMessageFormat = jsonFormat2(ConnectedMessage)

  implicit val moveRequestStateFormat: RootJsonFormat[MoveRequestState] = jsonFormat9(MoveRequestState)
  implicit val moveResultsStateFormat: RootJsonFormat[ResultsState] = jsonFormat6(ResultsState)
  implicit val moveRequestFormat: RootJsonFormat[MoveRequest] = jsonFormat4(MoveRequest)
  implicit val moveResultsFormat: RootJsonFormat[MoveResults] = jsonFormat4(MoveResults)
  implicit val connectedFormat: RootJsonFormat[Connected] = jsonFormat2(Connected)

  implicit object ResponseJsonFormat extends RootJsonFormat[Response] {
    def write(r: Response) = r match {
      case wait: WaitTurn => JsObject(
        "type" -> JsString("move"),
        "id"   -> JsNumber(wait.id.getOrElse(0)),
        "wait" -> JsBoolean(true)
      )
      case move: ChooseLocation => JsObject(
        "type"     -> JsString("move"),
        "id"       -> JsNumber(move.id.getOrElse(0)),
        "location" -> move.location.toJson
      )
      case _ => deserializationError("Unexpected")
    }


    // read isn't used but is required
    def read(value: JsValue) = deserializationError("Unexpected")
  }

}
