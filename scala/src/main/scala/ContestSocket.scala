import java.net._
import java.nio.ByteBuffer
import java.io._
import spray.json._
import DefaultJsonProtocol._
import ContestBotJsonProtocol._

object CudaBotMain extends App {
  def parseRequest(request: String): Request = {
    val jsonAst = JsonParser(request)

    jsonAst.asJsObject.getFields("type").head match {
      case JsString("request")     => jsonAst.convertTo[MoveRequest]
      case JsString("move_result") => jsonAst.convertTo[MoveResults]
      case JsString("error")       => {
        jsonAst.asJsObject.getFields("seen_host").head match {
          case _: JsString => jsonAst.convertTo[UnknownPlayerError]
        }
      }
      case JsString("connected") => {
        println(request)
        jsonAst.convertTo[Connected]
      }
      case _ => NoRequest()
    }
  }

  def run = {
    val server = "cuda.contest"
    val port = 9999
    val bot = new ContestBot

    println(s"Connecting to $server:$port")
    var socket = new Socket(server, port)
    val inputStream = socket.getInputStream
    val outputStream = socket.getOutputStream
    val writer = new DataOutputStream(outputStream)

    println("Connected")

    while (true) {
      // Read size of the request
      val sizeBuffer = new Array[Byte](4)
      inputStream.read(sizeBuffer, 0, 4)
      val size = ByteBuffer.wrap(sizeBuffer).getInt

      if (size > 0) {
        val requestBuffer = new Array[Byte](size)
        inputStream.read(requestBuffer, 0, size)
        val requestJson = new String(requestBuffer)
        val request_obj = parseRequest(requestJson)

        bot handleRequest request_obj match {
          case _: NoResponse => Nil
          case responseObj => {
            responseObj.id = request_obj match {
              case r: MoveRequest => r.id
              case _ => None
            }

            val responseJson = responseObj.toJson.toString

            if (responseJson != 1) {
              val responseBytes = responseJson.getBytes("UTF-8")
              val responseLength = responseBytes.length
              val sizeBytes = ByteBuffer.allocate(4).putInt(responseLength).array

              outputStream.write(sizeBytes)
              writer.write(responseBytes, 0, responseLength)
              writer.flush
            }
          }
        }
      } else throw new Exception
    }
  }

  while (true) {
    try { run }
    catch {
      case e: Throwable => {
        println("Caught Exception: " + e)
        Thread.sleep(10000)
      }
    }
  }
}
