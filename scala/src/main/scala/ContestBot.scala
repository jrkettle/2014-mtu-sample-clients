import ContestBotJsonProtocol._
import scala.util.Random

case class Score(player1: Int, player2: Int)

case class MoveRequestState(
  time_remaining_ns: Long,
  moves_remaining: Int,
  tokens: Int,
  opponent_id: Int,
  opponent_tokens: Int,
  player: Int,
  score: Score,
  board: List[List[List[Int]]],
  legal_moves: List[List[Int]]
)

case class ResultsState(
  claimed: Option[List[Int]],
  player: Int,
  score: Score,
  board: List[List[List[Int]]],
  tokens: Int,
  error: Option[String]
)

case class ConnectedMessage(
  team_id: Int,
  welcome: String
)

// Type is a keyword. Have to escape it with backtick
// Any request coming from the server will have a associated case class that extends Request
abstract class Request()
case class MoveRequest(`type`: String, game_id: Int, id: Option[Int], state: MoveRequestState) extends Request
case class UnknownPlayerError(`type`: String, message: String, seen_host: String) extends Request
case class Connected(`type`: String, message: ConnectedMessage) extends Request
case class NoRequest() extends Request
case class MoveResults(`type`: String, game_id: Int, id: Option[Int], state: ResultsState) extends Request

// Response classes
abstract class Response {var id: Option[Int]}

case class WaitTurn(var id: Option[Int] = None) extends Response
case class ChooseLocation(location: List[Int], var id: Option[Int] = None) extends Response
case class NoResponse(var id: Option[Int] = None) extends Response

// Bot logic goes in here...
class ContestBot() {
  var currentGameId = -1
  val rand = new Random(System.currentTimeMillis())

  def handleRequest(request: Request): Response = {
    request match {
      case r: MoveRequest => {
        if (currentGameId != r.game_id) {
          currentGameId = r.game_id
          println("New game: " + currentGameId)
        }

        val state = r.state
        if (rand.nextBoolean)
          WaitTurn()
        else {
          val rand_idx = rand.nextInt(state.legal_moves.length)
          ChooseLocation(state.legal_moves(rand_idx))
        }
      }
      case r: UnknownPlayerError => {
        println(r.message)
        System.exit(1);

        // Only necessary to appease the compiler
        NoResponse()
      }
      case _ => NoResponse()
    }
  }
}
